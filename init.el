;; https://github.com/bbatsov/prelude/issues/938
;; https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=803462
;; Always load newest byte code
;; (setq load-prefer-newer t)

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

(eval-when-compile
  (require 'use-package))

(require 'use-package-ensure)
(setq use-package-always-ensure t)

(use-package bind-key)
(use-package dash)
(use-package drag-stuff)
(use-package editorconfig)
(use-package exec-path-from-shell)
(use-package expand-region)
(use-package f)
(use-package flycheck
	     :init (global-flycheck-mode))
(add-hook 'after-init-hook #'global-flycheck-mode)
(use-package flycheck-pyflakes)
(use-package flycheck-rust)
(use-package flycheck-yamllint)
(use-package flymd)
(use-package htmlize)
(use-package git)
(use-package git-commit)
(use-package go-mode)
(use-package groovy-mode)
(use-package idle-highlight-mode)
(use-package kivy-mode)
(use-package magit)
(use-package markdown-mode)
(use-package multiple-cursors)
(use-package nyan-mode)
(use-package php-mode)
(use-package popwin)
(use-package prodigy)
(use-package projectile)
(use-package puppet-mode)
(use-package rust-mode)
(use-package s)
(use-package smartparens)
(use-package smex)
(use-package yaml-mode)
(use-package zig-mode)

;; Generic Emacs stuff
(add-to-list 'load-path "~/.emacs.d/modes/")
(add-to-list 'load-path "~/.emacs.d/misc/")
(show-paren-mode 1)

(setq-default css-indent-offset 2)

;; Always use spaces instead of tabs, a tab is 4 spaces by default
(setq-default tab-always-indent t)
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq-default tab-stop-list (number-sequence 4 120 4))
(setq-default tab-always-indent 'complete)

;; WhiteSpace hilighting
(require 'whitespace)
(setq whitespace-style (quote (face trailing tabs tab-mark empty lines-tail)))
(setq-default whitespace-line-column 80)
(global-whitespace-mode 1)

;; C, use k&r style by default (naev)
(setq-default c-default-style "k&r"
              c-basic-offset 3)

(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))

; Flymd's flyit doesn't work well with chrome/chromeium
;(defun my-flymd-browser-function (url)
;  (let ((browse-url-browser-function 'browse-url-firefox))
;    (browse-url url)))
;(setq flymd-browser-open-function 'my-flymd-browser-function)

; Magit configuration.
(global-set-key (kbd "C-x g") 'magit-status)

; Beancounter
(add-to-list 'load-path "~/src/misc/beancount/elisp")
(require 'beancount nil 'noerror)
(add-to-list 'auto-mode-alist '("\\.beancount\\'" . beancount-mode))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(zig-mode yaml-mode yanshippet smex smartparens rust-mode puppet-mode projectile prodigy popwin php-mode nyan-mode multiple-cursors markdown-mode magit kivy-mode idle-highlight-mode groovy-mode go-mode git-commit git htmlize flymd flycheck-yamllint flycheck-rust flycheck-pyflakes flycheck f expand-region exec-path-from-shell editorconfig drag-stuff dash cmake-mode use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
